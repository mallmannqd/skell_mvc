<html>
    <head>
        <title>Meu site</title>
        <link rel="stylesheet" href="<?=BASE_URL?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/style.css">
    </head>
    <body>
        <div class="container">
            <?php $this->loadView($viewName, $viewData); ?>
        </div>

        <script type="text/javascript" src="<?=BASE_URL?>assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="<?=BASE_URL?>assets/js/bootstrap.min.js"></script>
    </body>
</html>