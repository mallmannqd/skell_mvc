<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 08/04/18
 * Time: 15:30
 */

namespace core;


class Core
{
    private $routes;

    public function run()
    {
        $url = '/';

        if (isset($_GET['url'])){
            $url = $_GET['url'];
        }


	if(!empty($this->routes)){
	    $this->checkRoutes($url);
	}

        $params = [];
        if ($url != '/') {
            $url = explode('/', $url);
            array_shift($url);

            $currentController = 'controllers\\' . ucfirst($url[0]).'Controller';
            array_shift($url);

            if (isset($url) && !empty($url[0])){
                $currentAction = $url[0];
                array_shift($url);
            }else{
                $currentAction = 'index';
            }

            if (count($url) > 0){
                $params = $url;
            }


        }else {
            $currentController = 'controllers\IndexController';
            $currentAction = 'index';
        }

        if (!file_exists(str_replace('\\', '/', $currentController) . '.php') || !method_exists($currentController, $currentAction)){
            $currentController = 'controllers\ErrorsController';
            $currentAction = 'notFound';
        }

        $c = new $currentController();
        call_user_func_array([$c,$currentAction], $params);

    }

    public function setRoutes($routes): void
    {
        $this->routes = $routes;
    }

    private function checkRoutes(&$url)
    {
        foreach ($this->routes as $pt => $newurl){

            // Identifica os argumentos na chave do array de rotas e substitui por um padrão para regex
            $pattern = preg_replace('/\{[a-z0-9]{1,}\}/', '([a-z0-9-]{1,})', $pt);

            // Faz o match entre a url digitada e uma das rotas que foram definidas
            if (preg_match('#^(' . $pattern . ')*$#i', $url, $matches)){
                array_shift($matches);
                array_shift($matches);

                // Pega todos os argumentos para associar
                $itens = [];

                if (preg_match_all('/\{[a-z0-9]{1,}\}/', $pt, $m)){
                    $itens = preg_replace('(\{|\})', '', $m[0]);
                }

                //Faz a associação entre os argumentos e o valor
                $arg = [];
                foreach ($matches as $key => $match){
                    $arg[$itens[$key]] = $match;
                }

                foreach ($arg as $argkey => $argvalue) {
                    $newurl = str_replace(':'.$argkey, $argvalue, $newurl);
                }

                $url = $newurl;
                break;
            }

        }

    }
}
