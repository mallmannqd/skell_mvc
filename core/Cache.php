<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 13/04/18
 * Time: 15:42
 */

namespace core;


class Cache
{
    private $cache;

    public function setVar($nome, $valor): void
    {
        $this->readCache();
        $this->cache->$nome = $valor;
        $this->saveCache();
    }

    public function getVar($nome): String
    {
        $this->readCache();
        return $this->cache->$nome;
    }

    /**
     * @param $view
     * @return string
     */
    public function saveViewCache($view, $data): void
    {
        $this->validateCachePath($view);
        file_put_contents(CACHE_FOLDER . $view . '.cache', $data);
    }

    /**
     * @param $view
     * @return bool
     */
    public function viewExists($view): bool
    {
        if (file_exists(CACHE_FOLDER . $view . '.cache')) {
            return true;
        }

        return false;
    }

    /**
     * @param $view
     */

    public function loadViewCache($view): void
    {
        require CACHE_FOLDER . $view . '.cache';
    }

    public function isValid($view): bool
    {
        $last_modification = filectime(CACHE_FOLDER . $view . '.cache');
        $c = time() - $last_modification;

        if ($c > 10) {
            return false;
        }

        return true;

    }

    /**
     * @param $view
     * @return array
     */
    private function validateCachePath($view): void
    {
        $dir = explode('/', $view);
        $dir = $dir[0];

        if (!is_dir(CACHE_FOLDER . $dir)) {
            mkdir(CACHE_FOLDER . $dir);
        }
    }

    private function readCache(): void
    {
        $this->cache = new \stdClass();
        if (file_exists('/app/cache/cache.cache')){
            $this->cache = json_decode(file_get_contents('/app/cache/cache.cache'));
        }

    }

    private function saveCache(): void
    {
        file_put_contents("/app/cache/cache.cache", json_encode($this->cache));
    }

}