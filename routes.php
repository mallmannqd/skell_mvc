<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 18/04/18
 * Time: 14:26
 */

/**
 * Rotas de exemplo:
 *
 * $routes['/galeria/{id}/{titulo}'] = '/galeria/abrir/:id/:titulo';
 * $routes['/news/{id}'] = '/noticia/abrir/:id';
 * $routes['/{titulo}'] = '/noticia/abrir/:titulo';
 *
 */

$routes = [];
$routes['/paginator'] = '/examples/paginator';