<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 08/04/18
 * Time: 11:46
 */
use \core\Core as Core;

session_start();
require_once 'env.php';
require_once 'autoloader.php';

$core = new Core();

require_once 'routes.php';

$core->setRoutes($routes);
$core->run();